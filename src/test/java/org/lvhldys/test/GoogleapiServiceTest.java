package org.lvhldys.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lvhldys.model.Address;
import org.lvhldys.model.Constants;
import org.lvhlydys.service.GoogleapiServiceImpl;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

//@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext-test.xml"})
public class GoogleapiServiceTest extends AbstractJUnit4SpringContextTests {
	/*@Mock
	private RestTemplate restTemplate;
	
	@InjectMocks
	private GoogleapiServiceImpl googleApiService;*/
	
	@Autowired
    private GoogleapiServiceImpl googleApiService;

    @Autowired
    private RestTemplate restTemplate;
	
	
	MockRestServiceServer mockServer;
//	@Before
//	public void init() {
//		List<String> postCodes = new ArrayList<String>();
//		postCodes.add(Constants.POSTCODE_BT486DQ);
//		postCodes.add(Constants.POSTCODE_SW1A2AA);
//		postCodes.add(Constants.POSTCODE_W60LG);
//	}
	
	Map<String, String> expectedResults;
	String message_bt486dq;
	String message_sw1a2aa;
	String message_w60lg;
	@Before
	public void init() throws IOException {
		mockServer = MockRestServiceServer.createServer(restTemplate);
		
		message_bt486dq = readMessage(Constants.FILE_JSON_BT);
		message_sw1a2aa = readMessage(Constants.FILE_JSON_SW);
		message_w60lg = readMessage(Constants.FILE_JSON_W6);
		
		expectedResults = new HashMap<String, String>();
		expectedResults.put(Constants.POSTCODE_BT486DQ, "Londonderry BT48 6DQ, UK");
		expectedResults.put(Constants.POSTCODE_SW1A2AA, "London SW1A 2AA, UK");
		expectedResults.put(Constants.POSTCODE_W60LG, "Hammersmith Grove, London W6 0LG, UK");
	}
	
	private String readMessage(String fileName) throws IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		BufferedReader br = new BufferedReader(new FileReader(file));
		String result;
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    result = sb.toString();
		} finally {
		    br.close();
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Test
    public void testGetAddressFor_BT486DQ() {
        mockServer.expect(requestTo(Constants.BASE_URL+Constants.POSTCODE_BT486DQ))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(message_bt486dq, MediaType.APPLICATION_JSON));

        Address result = googleApiService.getAddressByPostCode(Constants.POSTCODE_BT486DQ);

        mockServer.verify();
        assertThat(result.getStreetName(), allOf(containsString(expectedResults.get(Constants.POSTCODE_BT486DQ))));
    }
	
	@SuppressWarnings("unchecked")
	@Test
    public void testGetAddressFor_SW1A2AA() {
        mockServer.expect(requestTo(Constants.BASE_URL+Constants.POSTCODE_SW1A2AA))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(message_sw1a2aa, MediaType.APPLICATION_JSON));

        Address result = googleApiService.getAddressByPostCode(Constants.POSTCODE_SW1A2AA);

        mockServer.verify();
        assertThat(result.getStreetName(), allOf(containsString(expectedResults.get(Constants.POSTCODE_SW1A2AA))));
    }
	
	@SuppressWarnings("unchecked")
	@Test
    public void testGetAddressFor_W60LG() {
        mockServer.expect(requestTo(Constants.BASE_URL+Constants.POSTCODE_W60LG))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(message_w60lg, MediaType.APPLICATION_JSON));

        Address result = googleApiService.getAddressByPostCode(Constants.POSTCODE_W60LG);

        mockServer.verify();
        assertThat(result.getStreetName(), allOf(containsString(expectedResults.get(Constants.POSTCODE_W60LG))));
    }
	
	@Test
	public void testGetAddressByNullPostCode() {
		assertTrue(googleApiService.getAddressByPostCode(null) == null);
		
	}
}
