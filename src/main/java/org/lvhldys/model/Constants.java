package org.lvhldys.model;

public class Constants {
	
	public static final String BASE_URL = "http://maps.googleapis.com/maps/api/geocode/json?address=";
	public static final String POSTCODE_BT486DQ = "BT486DQ";
	public static final String POSTCODE_W60LG = "W60LG";
	public static final String POSTCODE_SW1A2AA = "SW1A2AA";
	public static final String JSON_RESULTS = "results";
	public static final String JSON_FORMATTED_ADDRESS = "formatted_address";
	public static final String FILE_JSON_BT = "json_bt";
	public static final String FILE_JSON_SW = "json_sw";
	public static final String FILE_JSON_W6 = "json_w6";
}
