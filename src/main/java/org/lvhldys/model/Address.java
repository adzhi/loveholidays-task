package org.lvhldys.model;

public class Address {
	
	private final String streetName;

	public Address(String streetName) {
		super();
		this.streetName = streetName;
	}

	public String getStreetName() {
		return streetName;
	}
}
