package org.lvhldys.controller;

import java.util.ArrayList;
import java.util.List;

import org.lvhldys.model.Address;
import org.lvhldys.model.Constants;
import org.lvhlydys.service.GoogleapiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@Autowired
	GoogleapiService googleApiService;

	@RequestMapping("/")
	public List<Address> getStreetNames() {
		List<String> postCodes = new ArrayList<String>();
		postCodes.add(Constants.POSTCODE_BT486DQ);
		postCodes.add(Constants.POSTCODE_SW1A2AA);
		postCodes.add(Constants.POSTCODE_W60LG);
		return googleApiService.getAddresses(postCodes);
	}

	@RequestMapping(value = "/{postCode}", method = RequestMethod.GET)
	public Address getStreetNames(@PathVariable String postCode) {
		return googleApiService.getAddressByPostCode(postCode);
	}
}
