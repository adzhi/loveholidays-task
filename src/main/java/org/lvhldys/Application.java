package org.lvhldys;

import org.lvhlydys.service.GoogleapiService;
import org.lvhlydys.service.GoogleapiServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public GoogleapiService googleApiService() {
        return new GoogleapiServiceImpl();
    }

}
