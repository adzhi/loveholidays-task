package org.lvhlydys.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.lvhldys.model.Address;
import org.lvhldys.model.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class GoogleapiServiceImpl implements GoogleapiService {

	@Autowired
	RestTemplate restTemplate;

	public List<Address> getAddresses(List<String> postCodes) {
		List<Address> result = new ArrayList<Address>();
		PostcodeConsumer consumer = new PostcodeConsumer(result);
		postCodes.forEach(consumer);

		return result;
	}
	
	public Address getAddressByPostCode(String postCode) {
		String json = "";
		JSONArray jsonObject1;
		JSONObject jsonObject2;
		JSONParser parser = new JSONParser();
		Address a = null;
		
		if(postCode == null || postCode.isEmpty()) {
			return null; //primerno
		}

		try {
			json = restTemplate.getForObject(Constants.BASE_URL + postCode, String.class);
			JSONObject jsonObj = (JSONObject) parser.parse(json);
			jsonObject1 = (JSONArray) jsonObj.get(Constants.JSON_RESULTS);
			jsonObject2 = (JSONObject) jsonObject1.get(0);
			a = new Address(jsonObject2.get(Constants.JSON_FORMATTED_ADDRESS).toString());
		} catch (RestClientException e) {
			System.out.println("Problem occured while quering API: ");
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return a;
	}

	public class PostcodeConsumer implements Consumer<String> {
		List<Address> addresses;
		
		public PostcodeConsumer(List<Address> addresses) {
			super();
			this.addresses = addresses;
		}

		@Override
		public void accept(String postCode) {
			String json = "";
			JSONArray jsonObject1;
			JSONObject jsonObject2;
			JSONParser parser = new JSONParser();

			try {
				json = restTemplate.getForObject(Constants.BASE_URL + postCode, String.class);
				JSONObject jsonObj = (JSONObject) parser.parse(json);
				jsonObject1 = (JSONArray) jsonObj.get(Constants.JSON_RESULTS);
				jsonObject2 = (JSONObject) jsonObject1.get(0);
				Address a = new Address(jsonObject2.get(Constants.JSON_FORMATTED_ADDRESS).toString());
				addresses.add(a);
			} catch (RestClientException e) {
				System.out.println("Problem occured while quering API: ");
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
