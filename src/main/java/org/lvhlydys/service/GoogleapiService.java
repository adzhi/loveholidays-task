package org.lvhlydys.service;

import java.util.List;

import org.lvhldys.model.Address;
import org.springframework.stereotype.Service;

@Service
public interface GoogleapiService {
	
public List<Address> getAddresses(List<String> postCodes);

public Address getAddressByPostCode(String postCode);
}
