# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a task from the interview process at Loveholidays
### How do I get set up? ###

After clonning the project, navigate to the root directory and run

gradle build && java -jar build/libs/loveholidays-0.1.0.jar

after successfull build open the browser at: http://localhost:8080
this returns the addresses for the given postcodes (all 3 at once)

also you can use http://localhost:8080/{postcode}
which returns address for particular postcode